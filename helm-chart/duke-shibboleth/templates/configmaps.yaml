---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ printf "apconf-%s" .Release.Name | trunc 63 | trimSuffix "-" }}
  labels:
    helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
    app.kubernetes.io/name: proxy
    app.kubernetes.io/instance: {{ .Release.Name }}
data:
  servername.conf: |
    ServerName https://{{ .Values.entityID }}:443
    UseCanonicalName On
  {{- if .Values.applicationConfiguration }}
  application.conf: |
    {{ .Values.applicationConfiguration | nindent 4 }}
  {{- end }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ printf "shconf-%s" .Release.Name | trunc 63 | trimSuffix "-" }}
  labels:
    helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
    app.kubernetes.io/name: proxy
    app.kubernetes.io/instance: {{ .Release.Name }}
data:
  {{- if .Values.shibboleth.shibboleth2Xml }}
  shibboleth2.xml: |
    {{- .Values.shibboleth.shibboleth2Xml | nindent 4}}
  {{- else }}
  shibboleth2.xml: |
    <SPConfig xmlns="urn:mace:shibboleth:3.0:native:sp:config"
        xmlns:conf="urn:mace:shibboleth:3.0:native:sp:config"
        clockSkew="180">

        <OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" />

        <RequestMapper type="Native">
            <RequestMap applicationId="default" REMOTE_ADDR="X-Forwarded-For"/>
        </RequestMapper>

        <ApplicationDefaults entityID="https://{{ .Values.entityID }}"
            REMOTE_USER={{ quote .Values.shibboleth2XmlRemoteUser }}
            cipherSuites="DEFAULT:!EXP:!LOW:!aNULL:!eNULL:!DES:!IDEA:!SEED:!RC4:!3DES:!kRSA:!SSLv2:!SSLv3:!TLSv1:!TLSv1.1">


            <Sessions lifetime="28800" timeout="3600" relayState="ss:mem"
                      checkAddress="false" handlerSSL="false" cookieProps="https">

                <SSO entityID="https://shib.oit.duke.edu/shibboleth-idp"
                    outgoingBindings="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST">
                  SAML2
                </SSO>

                <Logout>SAML2 Local</Logout>

                <LogoutInitiator type="Admin" Location="/Logout/Admin" acl="127.0.0.1 ::1" />

                <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>

                <Handler type="Status" Location="/Status" acl="127.0.0.1 ::1"/>

                <Handler type="Session" Location="/Session" showAttributeValues="false"/>

                <Handler type="DiscoveryFeed" Location="/DiscoFeed"/>
            </Sessions>

            <Errors supportContact="root@localhost"
                helpLocation="/about.html"
                styleSheet="/shibboleth-sp/main.css"/>

            <MetadataProvider type="XML" validate="true"
                url="https://shib.oit.duke.edu/duke-metadata-2-signed.xml"
                backingFilePath="duke-metadata-2-signed.xml" reloadInterval="7200">
                <MetadataFilter type="Signature" certificate="idp_signing.crt"/>
            </MetadataProvider>

            <AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map.xml"/>

            <AttributeFilter type="XML" validate="true" path="attribute-policy.xml"/>

            <CredentialResolver type="File" key="tls.key" certificate="tls.crt"/>

        </ApplicationDefaults>

        <SecurityPolicyProvider type="XML" validate="true" path="security-policy.xml"/>

        <ProtocolProvider type="XML" validate="true" reloadChanges="false" path="protocols.xml"/>

    </SPConfig>

  {{- end }}
  {{- if .Values.shibboleth.attributeMap }}
  atrribute-map.xml: |
    {{- .Values.shibboleth.attributeMap | nindent 4}}
  {{- end }}
