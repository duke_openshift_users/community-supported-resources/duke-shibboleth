Deploy to Kubernetes with Helm
---

The duke-shibboleth helm chart will instantiate
apache and shibboleth containers in a Kubernetes cluster, according to
[These Instructions](https://openshift-docs.cloud.duke.edu/user-guide/dukeShibboleth/).

#### Usage

The chart requires the following values to work:

- entityID: This is used as the primary CN in the certificate, and
registered with the Shibboleth Site Management System with
the certificate. It is used to generate the shibboleth2.xml,
and the Route.spec.host.
- tls.crt: The certificate used to register the entityID with the
Shibboleth Site Management System. This can be set to the contents of a file
using the --set-file argument to helm.
- tls.key: The key to the cert. This can be set to the contents of a file
using the --set-file argument to helm.

The chart allows the following values to be supplied as well:
- useIngress(default True unless cluster has route.openshift.io/v1 capability): 'helm template' command defaults to a generic k8s server so we can't test using an OpenShift Route object. There may also be cases where we want an OpenShift-destined deployment to use Ingress instead of Route. When no option is given, and the cluster is OpenShift, the Chart will default to a Route
- vanityTlsCert and vanityTlsKey: File-type values for the front-end vanity hostname's TLS key and cert (not to be confused with the shibboleth key/cert) to be used by an Openshift route. Absent these values, the default is to use whatever TLS cert/key are on the default front-end.
- imageRegistry: path to the gitlab project image registry. Defaults to the official
doug community supported resources image
gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth
- registry: This is a hash, with the following values:
  - root: path to the gitlab image registry root. This defaults to gitlab-registry.oit.duke.edu.
  - secret: This should only be used for private forks of the official DOUG Community supported Resource. This is a hash, with the following values:
    - username: username to use to pull the image from the Gitlab Project
    registry. If you create a
    gitlab-deploy-token deployment token in the fork project,
    this is supplied in the CI_DEPLOY_USER environment variable.
    - password: Password for the user used to pull the image
    from the Gitlab Project Registry. If you create a
    gitlab-deploy-token deployment token in the fork project,
    this is supplied in the CI_DEPLOY_PASSWORD environment variable.
- applicationConfiguration: This can be set to the contents of a file
using the --set-file argument to helm. If present,
Its contents are mounted into the httpd container as /etc/httpd/
conf.d/application.conf, which will be loaded into the overall httpd
configuration.
- shibboleth.attributeMap: This can be set to the contents of a file
using the --set-file argument to helm. If present, its contents
are loaded into the shibboleth container as /etc/shibboleth/attribute-map.xml.
- git_commit: added as a label in the deployment.
- shibboleth2XmlRemoteUser: Changes the REMOTE_USER value in shibboleth2.xml. Defaults to "eppn subject-id pairwise-id persistent-id" for backward-compatibility. Note that if you also set shibboleth.shibboleth2Xml, this flag will be ignored.
- shibboleth.shibboleth2Xml: This can be set to the contents of a file using the --set-file argument to helm. If present, its contents are loaded into the shibboleth container as /etc/shibboleth/shibboleth2.xml. Note that this negates any effect of shibboleth2XmlRemoteUser.
- useIngress: set to 'true' to create an ingress for the shibboleth proxy service, *instead of a Route*. This will require that **tlsSecretName** be set as well (more below on tlsSecretName)
- tlsSecretName: Set this to the name of a tls secret that contains the TLS cert/key for the entityID hostname. The details and setup of the TLS secret are outside the scope of this document
- ingressPath: Set this to add a path element to the ingress for sites that are served at a path above '/'

```bash
helm upgrade --force --recreate-pods --debug \
--set entityID=${ENTITY_ID} \
--set-file tls.crt="${ENTITY_ID}.crt" \
--set-file tls.key="${ENTITY_ID}.key" \
--wait \
--install ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth
```

To add an attribute map, and an application.conf file, use the following command:
```bash
extraArgs='--set-file applicationConfiguration=application.conf-example --set-file shibboleth.attributeMap=attribute-map.xml-grouper-example'
helm upgrade --force --recreate-pods --debug \
--set entityID=${ENTITY_ID} \
--set-file tls.crt="${ENTITY_ID}.crt" \
--set-file tls.key="${ENTITY_ID}.key" \
${extraArgs} \
--wait \
--install ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth
```

To modify /etc/shibboleth/shibboleth2.xml, replacing REMOTE_USER with a custom string:
```bash
helm upgrade --force --recreate-pods --debug \
...
--set-string shibboleth2XmlRemoteUser="uid etCetera fooBar" \
...
```

To replace /etc/shibboleth/shibboleth2.xml completely with local file conf/shibboleth2.xml:
```bash
helm upgrade --force --recreate-pods --debug \
...
--set-file shibboleth.shibboleth2Xml="conf/shibboleth2.xml" \
...
```

To use an **Ingress** object instead of a **Route**, with optional path:
```bash
helm upgrade --force --recreate-pods --debug \
--set entityID=${ENTITY_ID} \
--set useIngress=true \
--set-string tlsSecretName=${TLS_SECRET_OBJECT_NAME} \
--set-string ingressPath="/path/to/app" \
--wait \
--install ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth
```

#### Useful Helm Commands

It is easiest (and cross Openshift/Kubernetes compatible) to run a
local instance of tiller for helm to use (at least until we upgrade
to helm 3). [Tillerless Helm](https://rimusz.net/tillerless-helm/)

You must have your oc or kubectl cli logged in to the cluster
you wish to deploy to for this to work.

```bash
export TILLER_NAMESPACE=$PROJECT_NAMESPACE
export HELM_HOST=localhost:44134
tiller --storage=secret &
export TILLER_PID=$!
sleep 1
kill -0 ${TILLER_PID}
if [ $? -gt 0 ]
then
  raise "tiller not running!"
fi
helm init --client-only
helm repo update
```

You can list which deployments are registered in your locally running tiller:
```
helm list
```

This can be useful to test if your session has expired. If it has, you
will likely see errors saying your user does not have the ability to
list things, e.g. the same errors you would get if you ran oc get all.

If you want to delete EVERYTHING for a deployment, use helm delete (the `--purge`
argument removes information stored by the locally running tiller). To delete
this helm chart, use the same CI_ENVIRONMENT_NAME that you used in the upgrade above:
```
helm delete --purge ${CI_ENVIRONMENT_NAME}
```

Helm allows you to lint a chart
```
helm lint helm-chart/duke-shibboleth
```

You can also print out all of the things that would be installed
for the chart using helm template (with or without the same --set and
--set-file arguments used in helm upgrade). The only difference is that
you need to supply a name to the template This would be the value you would
supply in CI_ENVIRONMENT_NAME in the the upgrade command.

```bash
helm template \
--set registry.root=${CI_REGISTRY} \
--set registry.secret.username=${CI_DEPLOY_USER} \
--set registry.secret.password="${CI_DEPLOY_PASSWORD}" \
--set entityID=${ENTITY_ID} \
--set-file tls.crt="${ENTITY_ID}.crt" \
--set-file tls.key="${ENTITY_ID}.key" \
--set imageRegistry="${CI_REGISTRY_IMAGE}" \
${extraArgs} \
-n ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth
```

Unfortunately, there is no way to print out a single template item.

You should kill the locally running tiller after you are finished
with it.

```bash
kill $TILLER_PID.
```
